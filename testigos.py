import sys, os, django
from tqdm import tqdm
from happy_import import save_list
from concurrent.futures.thread import ThreadPoolExecutor


os.environ['DJANGO_SETTINGS_MODULE'] = 'elecciones.settings'
django.setup()
from apps.votacion.models import Votodetalle, Indicador, Divipol, Partido, Corporacion, Circunscripcion, Candidato, Mmvs, MmvGhost
from django.http import HttpResponse

#listado = Mmvescrutinio1.objects.filter(votos=0)[:5]

#for x in listado:
 #   print(x)

iniciar = 1
departamento = 16
municipio = 1
corporacion = 3
circunscripcion = 2
mmvs_list = list()


def saveVotos(mmvs_list):
    save_list(mmvs_list)


def pushVoto(init, fraccion, lines, total_lines):
    print("entro init ", init, " a ", fraccion, " total lines ", total_lines)
    for i in tqdm(range(init, fraccion), desc="Importando " + str(total_lines) + " votos"):
        line = lines[i]
        codigopre = line[0:15] + line[22:29]
        codigopreinstance = Votodetalle.objects.filter(codigo=codigopre).first()
        if codigopreinstance:
            q = Mmvs.objects.get(codigo=codigopre)
            q.testigos=line[29:37]
            q.save()
        else:
            q = MmvGhost(codigo=codigopre,
                         votos=line[29:37],
                         tipo="testigos", )
            q.save()


class UploadEscrutinioDia1():
    if iniciar == 1:

        nombre_file = 'testigos.txt'
        print("Nombre de Archivo de Testigos <default:", nombre_file, ">")
        print("escribe el nombre o deja en blanco por defecto:", end="")
        nombre = input()
        if nombre == '' is False:
            nombre_file = nombre
        print('MMVs/' + nombre_file)
        file = open('MMVs/'+nombre_file, mode='r')
        lines = file.readlines()
        file.close()
        puestoid = 0
        lines_len = len(lines)
        fraccionar_en = 8
        fraccion = lines_len // fraccionar_en
        fracciones = fraccionar_en
        init = 1
        for i in range(1, fracciones):
            print("entro", str(i))
            pushVoto(init,
                     fraccion * i,
                     lines,
                     lines_len, )
            init = fraccion * i+1
        # para ejecutar la fraccion faltante
        pushVoto(init,
                 lines_len,
                 lines,
                 lines_len, )
        print("Se analizaron ", str(lines_len), " registros")

    def get(self, request):
        return HttpResponse("ok")




def pushVotoConcejo(init, fraccion, lines, total_lines):
    print("entro init ", init, " a ", fraccion, " total lines ", total_lines)
    for i in tqdm(range(init, fraccion), desc="Importando " + str(total_lines) + " votos"):
        line = lines[i]
        codigopre = line[0:15] + line[22:29]
        codigopreinstance = Votodetalle.objects.filter(codigo=codigopre).first()
        if codigopreinstance:
            q = Mmvsconcejo.objects.get(codigo=codigopre)
            q.testigos=line[29:37]
            q.save()
        else:
            q = MmvGhostconcejo(codigo=codigopre,
                         votos=line[29:37],
                         tipo="testigos", )
            q.save()


def UploadEscrutinioConcejoDia1():
    if iniciar == 1:

        nombre_file = 'testigos.txt'
        print("Nombre de Archivo de Testigos <default:", nombre_file, ">")
        print("escribe el nombre o deja en blanco por defecto:", end="")
        nombre = input()
        if nombre == '' is False:
            nombre_file = nombre
        print('MMVs/' + nombre_file)
        file = open('MMVs/'+nombre_file, mode='r')
        lines = file.readlines()
        file.close()
        puestoid = 0
        lines_len = len(lines)
        fraccionar_en = 8
        fraccion = lines_len // fraccionar_en
        fracciones = fraccionar_en
        init = 1
        with ThreadPoolExecutor() as executor:
            for i in range(1, fracciones):
                print("entro", str(i))
                executor.submit(pushVotoConcejo,
                                init,
                                fraccion * i,
                                lines,
                                lines_len, )
                init = fraccion * i+1
            # para ejecutar la fraccion faltante
            executor.submit(pushVotoConcejo,
                            init,
                            lines_len,
                            lines,
                            lines_len, )
        print("Finalizo Carga de ", str(lines_len), " registros")

    def get(self, request):
        return HttpResponse("ok")
from django.urls import path
from django.contrib.auth.views import login_required
from apps.votacion.views import IndicadorList, VerificacionList, ZonaList, ZonaDetail, ZonaDetailSimple

urlpatterns = [
	path('listar', IndicadorList.as_view(), name='votacion_listar'),
	path('verificar', VerificacionList, name='votacion_verificar'),
	path('zona', ZonaList, name='votacion_zona'),
	path('zonas', ZonaDetail, name='votacion_ver'),
  path('zonassimple', ZonaDetailSimple, name='votacion_ver_simple'),
]
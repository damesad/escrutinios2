from django import forms
from django.forms import ModelForm
from apps.votacion.models import Indicador

class IndicadorForm(ModelForm):
    class Meta:
        model = Indicador
        fields = [
            'codigo',
            'nombre',
            'potencial',
        ]
        labels = {
            'codigo': 'Codigo',
            'nombre': 'Nombre',
            'potencial': 'Potencial',
        }

        widgets = {
            'codigo': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'potencial': forms.TextInput(attrs={'class':'form-control'}),

        }

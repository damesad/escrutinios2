from django.db import models

class Indicador(models.Model):
    codigo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)
    potencial = models.IntegerField()

    def __str__(self):
        return self.nombre

class Partido(models.Model):
    codigo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

class Corporacion(models.Model):
    class Meta:
        verbose_name_plural = "Corporaciones"

    codigo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=200)

    def __str__(self):
        return self.nombre

class Circunscripcion(models.Model):
    codigo = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class Divipol(models.Model):
    idpuesto = models.IntegerField(primary_key=True)
    cod_dpto = models.IntegerField()
    cod_mun = models.IntegerField()
    cod_zona = models.IntegerField()
    cod_puesto = models.CharField(max_length=2)
    dpto_name = models.CharField(max_length=12)
    mun_name = models.CharField(max_length=30)
    puesto_name = models.CharField(max_length=40)
    indicador = models.ForeignKey(Indicador, null=True, blank=True, on_delete=models.DO_NOTHING)
    potencial_h = models.IntegerField()
    potencial_m = models.IntegerField()
    no_mesas = models.IntegerField()
    cod_comuna = models.IntegerField()
    comuna_name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.puesto_name)


class Candidato(models.Model):
    codigoc = models.CharField(max_length=7,null=False, blank=True, primary_key=True)
    corporacion = models.ForeignKey(Corporacion, null=True, blank=True, on_delete=models.DO_NOTHING)
    circunscripcion = models.ForeignKey(Circunscripcion, null=True, blank=True, on_delete=models.DO_NOTHING)
    cod_dpto = models.IntegerField()
    cod_mun = models.IntegerField()
    cod_comuna = models.IntegerField()
    cod_partido = models.ForeignKey(Partido, null=True, blank=True, on_delete=models.DO_NOTHING)
    preferente = models.IntegerField()
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    cedula = models.BigIntegerField()
    genero = models.CharField(max_length=1)
    sorteo = models.CharField(max_length=2)

    def __str__(self):
        return self.nombre


class Concejal(models.Model):
    codigoc = models.CharField(max_length=7,null=False, blank=True, primary_key=True)
    corporacion = models.ForeignKey(Corporacion, null=True, blank=True, on_delete=models.DO_NOTHING)
    circunscripcion = models.ForeignKey(Circunscripcion, null=True, blank=True, on_delete=models.DO_NOTHING)
    cod_dpto = models.IntegerField()
    cod_mun = models.IntegerField()
    cod_comuna = models.IntegerField()
    cod_partido = models.ForeignKey(Partido, null=True, blank=True, on_delete=models.DO_NOTHING)
    preferente = models.IntegerField()
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    cedula = models.BigIntegerField()
    genero = models.CharField(max_length=1)
    sorteo = models.CharField(max_length=2)

    def __str__(self):
        return self.nombre


class Votoconcejo(models.Model):
    codigo = models.CharField(max_length=25, primary_key=True)
    puesto = models.ForeignKey(Divipol, null=True, blank=True, on_delete=models.DO_NOTHING)
    no_mesa = models.IntegerField()
    cod_partido = models.IntegerField()
    cod_candidato = models.ForeignKey(Concejal, null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.codigo


class Mmvsconsejo(models.Model):
    codigo = models.OneToOneField(Votoconcejo, primary_key=True, on_delete=models.DO_NOTHING)
    testigos = models.IntegerField(default=None, null=True, blank=True)
    preconteo = models.IntegerField(default=None, null=True, blank=True)
    escrutinional1 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd1 = models.IntegerField(default=None, null=True, blank=True)
    escrutinional2 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd2 = models.IntegerField(default=None, null=True, blank=True)
    escrutinional3 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd3 = models.IntegerField(default=None, null=True, blank=True)
    escrutinional4 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd4 = models.IntegerField(default=None, null=True, blank=True)

    def __str__(self):
        return str(self.codigo)

    def get_zona(self):
        return str(self.codigo.puesto.cod_zona)
    get_zona.admin_order_field = 'codigo'

    def get_puesto(self):
        return str(self.codigo.puesto.puesto_name)

    def get_mesa(self):
        return str(self.codigo.no_mesa)

    def get_cod_candidato(self):
        return str(self.codigo.cod_candidato.nombre + self.codigo.cod_candidato.apellido)


class MmvGhostconcejo(models.Model):
    codigo = models.CharField(max_length=25, primary_key=True)
    votos = models.IntegerField()
    tipo = models.CharField(max_length=25,null=True, blank=True)

    def __str__(self):
        return self.codigo


class Votodetalle(models.Model):
    codigo = models.CharField(max_length=25, primary_key=True)
    puesto = models.ForeignKey(Divipol, null=True, blank=True, on_delete=models.DO_NOTHING)
    no_mesa = models.IntegerField()
    cod_partido = models.IntegerField()
    cod_candidato = models.ForeignKey(Candidato, null=True, blank=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.codigo


class MmvGhost(models.Model):
    codigo = models.CharField(max_length=25, primary_key=True)
    votos = models.IntegerField()
    tipo = models.CharField(max_length=25,null=True, blank=True)

    def __str__(self):
        return self.codigo


class Mmvs(models.Model):
    codigo = models.OneToOneField(Votodetalle, primary_key=True, on_delete=models.DO_NOTHING)
    testigos = models.IntegerField(default=None, null=True, blank=True)
    preconteo = models.IntegerField(default=None, null=True, blank=True)
    escrutinional1 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd1 = models.IntegerField(default=None, null=True, blank=True)
    escrutinional2 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd2 = models.IntegerField(default=None, null=True, blank=True)
    escrutinional3 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd3 = models.IntegerField(default=None, null=True, blank=True)
    escrutinional4 = models.IntegerField(default=None, null=True, blank=True)
    escrutiniocd4 = models.IntegerField(default=None, null=True, blank=True)

    def __str__(self):
        return str(self.codigo)

    def get_zona(self):
        return str(self.codigo.puesto.cod_zona)
    get_zona.admin_order_field = 'codigo'

    def get_puesto(self):
        return str(self.codigo.puesto.puesto_name)

    def get_mesa(self):
        return str(self.codigo.no_mesa)

    def get_cod_candidato(self):
        return str(self.codigo.cod_candidato.nombre + self.codigo.cod_candidato.apellido)


class ReporteDiferencia(Mmvs):

    class Meta:
        proxy = True



class ReporteMenore(Mmvs):

    class Meta:
        proxy = True
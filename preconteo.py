import sys, os, django
from tqdm import tqdm
from happy_import import save_list
import threading
from concurrent.futures.thread import ThreadPoolExecutor
from tqdm import tqdm


os.environ['DJANGO_SETTINGS_MODULE'] = 'elecciones.settings'
django.setup()
from apps.votacion.models import Votodetalle,  Mmvs, MmvGhost
from django.http import HttpResponse

#listado = Mmvescrutinio1.objects.filter(votos=0)[:5]

#for x in listado:
 #   print(x)

iniciar = 1
departamento = 16
municipio = 1
corporacion = 3
circunscripcion = 2
mmvs_list = list()


def pushVoto(init, fraccion, lines, total_lines):
    print("entro init ", init, " a ", fraccion, " total lines ", total_lines)
    for i in tqdm(range(init, fraccion), desc="Importando " + str(total_lines) + " votos"):
        line = lines[i]
        codigopre = line[0:15] + line[22:29]
        codigopreinstance = Votodetalle.objects.filter(codigo=codigopre).first()
        if codigopreinstance:
            q = Mmvs(codigo=codigopreinstance,
                     preconteo=line[29:37], )
            q.save()
        else:
            q = MmvGhost(codigo=codigopre,
                         votos=line[29:37],
                         tipo="preconteo", )
            q.save()


class UploadPreconteo():
    if iniciar == 1:
        nombre_file = 'ALC_MMV_16_9999.txt'
        print("Nombre de Archivo de Preconteo <default:", nombre_file, ">")
        print("escribe el nombre o deja en blanco por defecto:", end="")
        nombre = input()
        if nombre == '' is False:
            nombre_file = nombre
        file = open('MMVs/'+nombre_file, mode='r')
        lines = file.readlines()
        file.close()
        pre_count_mmvs = Mmvs.objects.all().count()
        puestoid = 0
        lines_len = len(lines) 
        fraccionar_en = 16
        fraccion = lines_len // fraccionar_en
        fracciones = fraccionar_en-1
        init = 1
        with ThreadPoolExecutor() as executor:
            for i in range(1, fracciones):
                executor.submit(pushVoto,
                                init,
                                fraccion * i,
                                lines,
                                lines_len, )
                init = fraccion * i+1
            # para ejecutar la fraccion faltante
            executor.submit(pushVoto,
                            init,
                            lines_len,
                            lines,
                            lines_len, )
        count_mmvs_ghost = MmvGhost.objects.filter(tipo="preconteo").count()
        count_mmvs = Mmvs.objects.all().count() - pre_count_mmvs
        print("Importacion Finalizada, MmvGhost tiene ",  count_mmvs_ghost, ' Registros')
        print("Importacion Finalizada, Mmvs tiene ",  count_mmvs, ' Registros')

    def get(self, request):
        return HttpResponse("ok")
